export interface IStudent {
    name: string;
    goals: number;
}
