import { Component, OnInit } from '@angular/core';

import { IStudent } from '../../interfaces/students.interface';
import { apiResponse } from '../../api/students.api';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {
  public students: IStudent[];

  constructor() {
    this.students = [];
   }

  ngOnInit() {
    this.students = apiResponse;
  }
}
