import { Component, OnInit } from '@angular/core';

import { ICharacter } from '../../interfaces/character.interface';
import { apiResponse } from '../../api/characters.api';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {
  public characters: ICharacter[];

  constructor() {
    // Give a default initial value to the variable to prevent breaking the render
    this.characters = [];
  }

  ngOnInit() {
    // If this was a request, the place to set the variable
    // is ngOnInit to prevent delayed rendering
    this.characters = apiResponse;
  }
}
